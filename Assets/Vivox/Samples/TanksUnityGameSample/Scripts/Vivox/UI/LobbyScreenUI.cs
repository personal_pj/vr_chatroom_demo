﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VivoxUnity;

public class LobbyScreenUI : MonoBehaviour
{

    private VivoxVoiceManager _vivoxVoiceManager;
    private DarkNetworkManager _vivoxNetworkManager;


    private EventSystem _evtSystem;

    public GameObject LobbyScreen;

    #region Unity Callbacks

    private void Awake()
    {
        _evtSystem = FindObjectOfType<EventSystem>();
        _vivoxVoiceManager = VivoxVoiceManager.Instance;
        _vivoxNetworkManager = FindObjectOfType<DarkNetworkManager>();
        _vivoxVoiceManager.OnUserLoggedInEvent += OnUserLoggedIn;
        _vivoxVoiceManager.OnUserLoggedOutEvent += OnUserLoggedOut;

        if (_vivoxVoiceManager.LoginState == LoginState.LoggedIn)
        {
            OnUserLoggedIn();
        }
        else
        {
            OnUserLoggedOut();
        }
    }

    private void OnDestroy()
    {
        _vivoxVoiceManager.OnUserLoggedInEvent -= OnUserLoggedIn;
        _vivoxVoiceManager.OnUserLoggedOutEvent -= OnUserLoggedOut;

    }

    #endregion

    private void JoinLobbyChannel()
    {
        _vivoxVoiceManager.JoinChannel(_vivoxNetworkManager.LobbyChannelName, ChannelType.NonPositional, VivoxVoiceManager.ChatCapability.TextAndAudio);
    }

    public void LogoutOfVivoxService()
    {
        _vivoxVoiceManager.DisconnectAllChannels();

        _vivoxVoiceManager.Logout();
    }

    #region Vivox Callbacks

    private void OnUserLoggedIn()
    {
        LobbyScreen.SetActive(true);
        var lobbychannel = _vivoxVoiceManager.ActiveChannels.FirstOrDefault(ac => ac.Channel.Name == _vivoxNetworkManager.LobbyChannelName);
        if ((_vivoxVoiceManager && _vivoxVoiceManager.ActiveChannels.Count == 0)
            || lobbychannel == null)
        {
            JoinLobbyChannel();
        }
        else
        {
            lobbychannel.BeginSetAudioConnected(true, TransmitPolicy.Yes, ar =>
            {
                Debug.Log("Now transmitting into lobby channel");
            });

        }
    }

    private void OnUserLoggedOut()
    {
        _vivoxVoiceManager.DisconnectAllChannels();

        LobbyScreen.SetActive(false);
    }

    #endregion
}
