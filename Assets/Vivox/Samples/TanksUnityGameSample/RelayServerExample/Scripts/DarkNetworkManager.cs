﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;
using VivoxUnity;
using System.Collections;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using UnityEngine.UI;

public class DarkNetworkManager : NetworkManager
{
    public enum MatchStatus
    {
        Open,
        Closed,
        Seeking
    }

    public DarkReflectiveMirrorTransport darkReflectiveMirrorTransport;
    public static DarkNetworkManager instance; 
    public string PositionalChannelName { get; private set; }
    public string LobbyChannelName;
    public Text roomname;
    private VivoxVoiceManager m_vivoxVoiceManager;

    #region Unity Callbacks

    public override void Awake()
    {
        base.Awake();
        m_vivoxVoiceManager = VivoxVoiceManager.Instance;
    }
    public string clientLocalIpAddress
    {
        get
        {
            string localIpAddress = null;
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530); //Google public DNS and port
                IPEndPoint localEndPoint = socket.LocalEndPoint as IPEndPoint;
                localIpAddress = localEndPoint.Address.ToString();
            }
            if (string.IsNullOrWhiteSpace(localIpAddress))
            {
                Debug.LogError("Unable to find clientLocalIpAddress");
            }
            return localIpAddress;
        }
    }
    public void filledroomname()
    {
        LobbyChannelName = roomname.text;
    }
    public string clientPublicIpAddress
    {
        get
        {
            string publicIpAddress = new WebClient().DownloadString("https://api.ipify.org");
            if (string.IsNullOrWhiteSpace(publicIpAddress))
            {
                Debug.LogError("Unable to find clientPublicIpAddress");
            }
            return publicIpAddress.Trim();
        }
    }

    public string HostingIp
    {
        get
        {
            if (NetworkServer.active || NetworkClient.serverIp == "localhost")
            {
                return clientLocalIpAddress;
            }
            // Your connected to a remote host. Let's get their ip
            return NetworkClient.serverIp ?? clientLocalIpAddress;
        }
    }
    public void SendLobbyUpdate(VivoxNetworkManager.MatchStatus status)
    {
        var lobbyChannelSession = m_vivoxVoiceManager.ActiveChannels.FirstOrDefault(ac => ac.Channel.Name == LobbyChannelName);
        if (lobbyChannelSession != null)
        {
            // NB: the message in the first argument will never get printed and is just for readability in the logs
            m_vivoxVoiceManager.SendTextMessage($"<{m_vivoxVoiceManager.LoginSession.LoginSessionId.DisplayName}:{status}>", lobbyChannelSession.Key, $"MatchStatus:{status}", (status == VivoxNetworkManager.MatchStatus.Open ? HostingIp : "blank"));
        }
        else
        {
            Debug.LogError($"Cannot send MatusStatus.{status}: not joined to {LobbyChannelName}");
        }
    }
    public void LeaveAllChannels(bool includeLobby = true)
    {
        foreach (var channelSession in m_vivoxVoiceManager.ActiveChannels)
        {
            if (channelSession.AudioState == ConnectionState.Connected || channelSession.TextState == ConnectionState.Connected
                && (includeLobby || (includeLobby == false && channelSession.Channel.Name != LobbyChannelName)))
            {
                channelSession.Disconnect();
            }
        }
    }
    private void VivoxVoiceManager_OnParticipantAddedEvent(string username, ChannelId channel, IParticipant participant)
    {
        if (channel.Name == PositionalChannelName && participant.IsSelf)
        {
            StartCoroutine(AwaitLobbyRejoin());
        }
        else if (channel.Name == LobbyChannelName && participant.IsSelf && NetworkServer.active)
        {
            // if joined the lobby channel and we're hosting a match
            // we should send an update since we could've missed a request when briefly out of channel
            SendLobbyUpdate(VivoxNetworkManager.MatchStatus.Open);
        }
    }

    private void VivoxVoiceManager_OnTextMessageLogReceivedEvent(string sender, IChannelTextMessage channelTextMessage)
    {
        if (channelTextMessage.ApplicationStanzaNamespace.EndsWith(VivoxNetworkManager.MatchStatus.Seeking.ToString()) && NetworkServer.active)
        {
            SendLobbyUpdate(VivoxNetworkManager.MatchStatus.Open);
        }
    }

    private IEnumerator AwaitLobbyRejoin()
    {
        IChannelSession lobbyChannel = m_vivoxVoiceManager.ActiveChannels.FirstOrDefault(ac => ac.Channel.Name == LobbyChannelName);
        // Lets wait until we have left the lobby channel before tyring to join it.
        yield return new WaitUntil(() => lobbyChannel == null
        || (lobbyChannel.AudioState != ConnectionState.Connected && lobbyChannel.TextState != ConnectionState.Connected));

        // Always send if hosting, since we could have missed a request.
        m_vivoxVoiceManager.JoinChannel(LobbyChannelName, ChannelType.NonPositional, VivoxVoiceManager.ChatCapability.TextOnly, VivoxUnity.TransmitPolicy.No);
    }
    public override void OnValidate()
    {
        base.OnValidate();
    }

    /// <summary>
    /// Runs on both Server and Client
    /// Networking is NOT initialized when this fires
    /// </summary>

    /// <summary>
    /// Runs on both Server and Client
    /// Networking is NOT initialized when this fires
    /// </summary>
    public override void Start()
    {
        base.Start();
    }

    public override void LateUpdate()
    {
        base.LateUpdate();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    #endregion

    #region Start & Stop

    /// <summary>
    /// Set the frame rate for a headless server.
    /// <para>Override if you wish to disable the behavior or set your own tick rate.</para>
    /// </summary>
    public override void ConfigureServerFrameRate()
    {
        base.ConfigureServerFrameRate();
    }

    /// <summary>
    /// called when quitting the application by closing the window / pressing stop in the editor
    /// <para>virtual so that inheriting classes' OnApplicationQuit() can call base.OnApplicationQuit() too</para>
    /// </summary>
    public override void OnApplicationQuit()
    {
        base.OnApplicationQuit();
    }

    #endregion

    #region Scene Management

    /// <summary>
    /// This causes the server to switch scenes and sets the networkSceneName.
    /// <para>Clients that connect to this server will automatically switch to this scene. This is called autmatically if onlineScene or offlineScene are set, but it can be called from user code to switch scenes again while the game is in progress. This automatically sets clients to be not-ready. The clients must call NetworkClient.Ready() again to participate in the new scene.</para>
    /// </summary>
    /// <param name="newSceneName"></param>
    public override void ServerChangeScene(string newSceneName)
    {
        base.ServerChangeScene(newSceneName);
    }

    #endregion

    #region Server System Callbacks

    /// <summary>
    /// Called on the server when a new client connects.
    /// <para>Unity calls this on the Server when a Client connects to the Server. Use an override to tell the NetworkManager what to do when a client connects to the server.</para>
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    public override void OnServerConnect(NetworkConnection conn) { }

    /// <summary>
    /// Called on the server when a client is ready.
    /// <para>The default implementation of this function calls NetworkServer.SetClientReady() to continue the network setup process.</para>
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    public override void OnServerReady(NetworkConnection conn)
    {
        base.OnServerReady(conn);
    }

    /// <summary>
    /// Called on the server when a client disconnects.
    /// <para>This is called on the Server when a Client disconnects from the Server. Use an override to decide what should happen when a disconnection is detected.</para>
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
    }

    /// <summary>
    /// Called on the server when a client adds a new player with ClientScene.AddPlayer.
    /// <para>The default implementation for this function creates a new player object from the playerPrefab.</para>
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    public override void OnServerAddPlayer(NetworkConnection conn, AddPlayerMessage msg)
    {
        base.OnServerAddPlayer(conn, msg);
    }

    /// <summary>
    /// Called on the server when a network error occurs for a client connection.
    /// </summary>
    /// <param name="conn">Connection from client.</param>
    /// <param name="errorCode">Error code.</param>
    public override void OnServerError(NetworkConnection conn, int errorCode) { }

    /// <summary>
    /// Called from ServerChangeScene immediately before SceneManager.LoadSceneAsync is executed
    /// <para>This allows server to do work / cleanup / prep before the scene changes.</para>
    /// </summary>
    /// <param name="newSceneName">Name of the scene that's about to be loaded</param>
    public override void OnServerChangeScene(string newSceneName) { }

    /// <summary>
    /// Called on the server when a scene is completed loaded, when the scene load was initiated by the server with ServerChangeScene().
    /// </summary>
    /// <param name="sceneName">The name of the new scene.</param>
    public override void OnServerSceneChanged(string sceneName) { }

    #endregion

    #region Client System Callbacks

    /// <summary>
    /// Called on the client when connected to a server.
    /// <para>The default implementation of this function sets the client as ready and adds a player. Override the function to dictate what happens when the client connects.</para>
    /// </summary>
    /// <param name="conn">Connection to the server.</param>
    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
    }

    /// <summary>
    /// Called on clients when disconnected from a server.
    /// <para>This is called on the client when it disconnects from the server. Override this function to decide what happens when the client disconnects.</para>
    /// </summary>
    /// <param name="conn">Connection to the server.</param>
    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
    }

    /// <summary>
    /// Called on clients when a network error occurs.
    /// </summary>
    /// <param name="conn">Connection to a server.</param>
    /// <param name="errorCode">Error code.</param>
    public override void OnClientError(NetworkConnection conn, int errorCode) { }

    /// <summary>
    /// Called on clients when a servers tells the client it is no longer ready.
    /// <para>This is commonly used when switching scenes.</para>
    /// </summary>
    /// <param name="conn">Connection to the server.</param>
    public override void OnClientNotReady(NetworkConnection conn) { }

    /// <summary>
    /// Called from ClientChangeScene immediately before SceneManager.LoadSceneAsync is executed
    /// <para>This allows client to do work / cleanup / prep before the scene changes.</para>
    /// </summary>
    /// <param name="newSceneName">Name of the scene that's about to be loaded</param>
    /// <param name="sceneOperation">Scene operation that's about to happen</param>
    /// <param name="customHandling">true to indicate that scene loading will be handled through overrides</param>
    public override void OnClientChangeScene(string newSceneName, SceneOperation sceneOperation, bool customHandling) { }

    /// <summary>
    /// Called on clients when a scene has completed loaded, when the scene load was initiated by the server.
    /// <para>Scene changes can cause player objects to be destroyed. The default implementation of OnClientSceneChanged in the NetworkManager is to add a player object for the connection if no player object exists.</para>
    /// </summary>
    /// <param name="conn">The network connection that the scene change message arrived on.</param>
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
    }

    #endregion

    #region Start & Stop Callbacks

    // Since there are multiple versions of StartServer, StartClient and StartHost, to reliably customize
    // their functionality, users would need override all the versions. Instead these callbacks are invoked
    // from all versions, so users only need to implement this one case.

    /// <summary>
    /// This is invoked when a host is started.
    /// <para>StartHost has multiple signatures, but they all cause this hook to be called.</para>
    /// </summary>
    public override void OnStartHost() { }

    /// <summary>
    /// This is invoked when a server is started - including when a host is started.
    /// <para>StartServer has multiple signatures, but they all cause this hook to be called.</para>
    /// </summary>
    public override void OnStartServer() { }

    /// <summary>
    /// This is invoked when the client is started.
    /// </summary>
    public override void OnStartClient()
    {
        Debug.Log("Starting client");
        LeaveAllChannels(true);
        PositionalChannelName = "Positional" + LobbyChannelName;
        m_vivoxVoiceManager.OnParticipantAddedEvent += VivoxVoiceManager_OnParticipantAddedEvent;
        m_vivoxVoiceManager.OnTextMessageLogReceivedEvent += VivoxVoiceManager_OnTextMessageLogReceivedEvent;
        base.OnStartClient();
    }

    /// <summary>
    /// This is called when a server is stopped - including when a host is stopped.
    /// </summary>
    public override void OnStopServer() { }

    /// <summary>
    /// This is called when a client is stopped.
    /// </summary>
    public override void OnStopClient()
    {
        Debug.Log("Stopping client");
        m_vivoxVoiceManager.OnParticipantAddedEvent -= VivoxVoiceManager_OnParticipantAddedEvent;
        m_vivoxVoiceManager.OnTextMessageLogReceivedEvent -= VivoxVoiceManager_OnTextMessageLogReceivedEvent;
        LeaveAllChannels(true);
        base.OnStopClient();
    }

    /// <summary>
    /// This is called when a host is stopped.
    /// </summary>
    public override void OnStopHost() { }

    #endregion

    public void UpdateServerData(string serverData)
    {
        //i am using this here because in ServerChangeScene, there is a timing problem 
        //which can give you previous scene name
        darkReflectiveMirrorTransport.UpdateServerData(serverData, maxConnections);
    }
}