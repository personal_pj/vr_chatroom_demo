﻿using System.Linq;
using UnityEngine;

public class VivoxTankHelper : MonoBehaviour
{
    public static PlayerSetup FindTankSetupInSceneByAccountId(string AccountID)
    {
		var tankSetups = FindObjectsOfType<PlayerSetup>();
		return tankSetups?.FirstOrDefault(ts => ts.m_AccountID == AccountID);
    }
}
