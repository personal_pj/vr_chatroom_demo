﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DarkRift;
using DarkRift.Client.Unity;
using Mirror;
using VivoxUnity;
using System;

public class DarkListServer : MonoBehaviour
{
    [Header("Transport's Info")]
    public DarkReflectiveMirrorTransport darkReflectiveMirrorTransport;
    public UnityClient unityClient;

    [Header("UI")]
    public Color onlinecolor;
    public GameObject mainPanel;
    public Transform content;
   // public Text statusText;
    public UIDarkServerStatusSlot slotPrefab;
    public Button serverAndPlayButton;
   // public Button serverOnlyButton;
    public GameObject connectingPanel;
    public Text connectingText;
    public Button connectingCancelButton;

    bool RelayConnected() => unityClient.ConnectionState == DarkRift.ConnectionState.Connected;
    bool IsConnecting() => NetworkClient.active && !ClientScene.ready;
    bool FullyConnected() => NetworkClient.active && ClientScene.ready;

    [Header("Refresh Settings")]
    public KeyCode refreshKey;
    public bool refreshOnStart = true;
    public int refreshInterval = 10; //0 To Disable

    private NetworkManagerHUD networkManagerHUD;
    private DarkNetworkManager _vivoxNetworkManager;
    private VivoxVoiceManager _vivoxVoiceManager;

    private void Awake()
    {
        _vivoxNetworkManager = FindObjectOfType<DarkNetworkManager>();
        if (!_vivoxNetworkManager)
        {
            Debug.LogError("Unable to find VivoxNetworkManager object.");
        }
        _vivoxVoiceManager = VivoxVoiceManager.Instance;
        _vivoxVoiceManager.OnParticipantAddedEvent += OnParticipantAdded;
    }
    private void OnDestroy()
    {
        _vivoxVoiceManager.OnParticipantAddedEvent -= OnParticipantAdded;
    }
    #region Vivox Callbacks

    private void OnParticipantAdded(string username, ChannelId channel, IParticipant participant)
    {
    }

    #endregion
    void Start()
    {
        networkManagerHUD = GetComponent<NetworkManagerHUD>();

        serverAndPlayButton.onClick.RemoveAllListeners();
        serverAndPlayButton.onClick.AddListener(() =>
        {
            _vivoxNetworkManager.LeaveAllChannels();
            NetworkManager.singleton.StartHost();
        });

        //serverOnlyButton.onClick.RemoveAllListeners();
       // serverOnlyButton.onClick.AddListener(() =>
        //{
        //    NetworkManager.singleton.StartServer();
        //});

        if (refreshOnStart)
        {
            RequestServersList();
        }

        if (refreshInterval > 0)
        {
            InvokeRepeating(nameof(RequestServersList), Time.realtimeSinceStartup + refreshInterval, refreshInterval);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (RelayConnected() == false)
        {
            connectingText.text = "No Relay Server Running";

            connectingCancelButton.onClick.RemoveAllListeners();
            connectingCancelButton.onClick.AddListener(() => connectingPanel.SetActive(false));
            return;
        }

        if (Input.GetKeyDown(refreshKey))
        {
            RequestServersList();
        }

        OnUI();
    }

    public void RequestServersList()
    {
        // You can remove these 2 statements it acts as a loading stuff
        connectingPanel.SetActive(true);
        connectingText.text = "Loading Server's Info from Relay Server";

        darkReflectiveMirrorTransport.RequestServerList();
    }

    public void RefreshServersList()
    {
        connectingPanel.SetActive(false);
        connectingText.text = "";
    }

    // instantiate/remove enough prefabs to match amount
    public void BalancePrefabs(GameObject prefab, int amount, Transform parent)
    {
        // instantiate until amount
        for (int i = parent.childCount; i < amount; ++i)
        {
            Instantiate(prefab, parent, false);
        }

        // delete everything that's too much
        // (backwards loop because Destroy changes childCount)
        for (int i = parent.childCount - 1; i >= amount; --i)
            Destroy(parent.GetChild(i).gameObject);
    }

    void OnUI()
    {
        // only show while client not connected and server not started
        if (!NetworkManager.singleton.isNetworkActive || IsConnecting())
        {
            mainPanel.SetActive(true);

            // instantiate/destroy enough slots
            BalancePrefabs(slotPrefab.gameObject, darkReflectiveMirrorTransport.relayServerList.Count, content);

            // refresh all members
            for (int i = 0; i < darkReflectiveMirrorTransport.relayServerList.Count; ++i)
            {
                UIDarkServerStatusSlot uIDarkServerStatusSlot = content.GetChild(i).GetComponent<UIDarkServerStatusSlot>();

                if (uIDarkServerStatusSlot != null)
                {
                    // use this for the title of server
                     uIDarkServerStatusSlot.titleText.text = darkReflectiveMirrorTransport.relayServerList[i].serverName;

                    // i use this to combine the extra variable stuff,[serverData] is extra Variable
                    //uIDarkServerStatusSlot.titleText.text = darkReflectiveMirrorTransport.relayServerList[i].serverName +
                    //   " " + darkReflectiveMirrorTransport.relayServerList[i].serverData;

                    uIDarkServerStatusSlot.playersText.text = darkReflectiveMirrorTransport.relayServerList[i].currentPlayers
                        + "/" + darkReflectiveMirrorTransport.relayServerList[i].maxPlayers;

                    //u can hide it
                    //uIDarkServerStatusSlot.addressText.text = darkReflectiveMirrorTransport.relayServerList[i].serverID.ToString();

                    ushort serverID = darkReflectiveMirrorTransport.relayServerList[i].serverID;

                    uIDarkServerStatusSlot.joinButton.onClick.RemoveAllListeners();
                    uIDarkServerStatusSlot.joinButton.onClick.AddListener(() =>
                    {
                        NetworkManager.singleton.networkAddress = serverID.ToString();
                        _vivoxNetworkManager.LeaveAllChannels();
                        NetworkManager.singleton.StartClient();
                    });
                }
            }
        }
        else mainPanel.SetActive(false);

        if (IsConnecting())
        {
            connectingPanel.SetActive(true);
            connectingText.text = "Connecting";

            // cancel button
            connectingCancelButton.onClick.RemoveAllListeners();
            connectingCancelButton.onClick.AddListener(NetworkManager.singleton.StopClient);
        }
        else connectingPanel.SetActive(false);

        if (FullyConnected())
        {
            networkManagerHUD.showGUI = true;
        }
        else { networkManagerHUD.showGUI = false; }

        //if (RelayConnected())
       // {
       //     statusText.text = "Online";
       //     statusText.color = onlinecolor;
       // }
      //  else { statusText.text = "Offline"; }
    }

    void OnApplicationQuit()
    {
        unityClient.Disconnect();
    }
}
