﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneControl : MonoBehaviour
{
    public GameObject loginScene;
    public GameObject registerScene;
    public GameObject resetScene;
    public GameObject confirmScene;
    public string testing;


    public GameObject lobbyscreen;
    public GameObject createscreen;
    public GameObject players;
    public GameObject settingscreen;
    public GameObject profilescreen;
    public GameObject namechangescreen;
    public GameObject passchangescreen;
    public GameObject emailchangescreen;
    public GameObject aboutscreen;

    void Start()
    {
        hideall();
    }
    private void hideall()
    {
        registerScene.SetActive(false);
        resetScene.SetActive(false);
        confirmScene.SetActive(false);
        createscreen.SetActive(false);
        players.SetActive(false);
        settingscreen.SetActive(false);
        profilescreen.SetActive(false);
        namechangescreen.SetActive(false);
        passchangescreen.SetActive(false);
        emailchangescreen.SetActive(false);
        aboutscreen.SetActive(false);
    }
    public void homescreen()
    {   
        hideall();
        lobbyscreen.SetActive(true);
    }
    public void createroom()
    {
        hideall();
        createscreen.SetActive(true);
    }
    public void playerscheck()
    {
        hideall();
        players.SetActive(true);
    }
    public void setting()
    {
        hideall();
        settingscreen.SetActive(true);
    }
    public void profile()
    {
        hideall();
        profilescreen.SetActive(true);
    }
    public void namechange()
    {
        hideall();
        namechangescreen.SetActive(true);
    }
    public void passchange()
    {
        hideall();
        passchangescreen.SetActive(true);
    }
    public void emailchange()
    {
        hideall();
        emailchangescreen.SetActive(true);
    }
    public void about()
    {
        hideall();
        aboutscreen.SetActive(true);
    }
    public void backcreateroom()
    {
        createscreen.SetActive(false);
        homescreen();
    }
    public void backplayerscheck()
    {
        players.SetActive(false);
        homescreen();
    }
    public void backsetting()
    {
        homescreen();
        settingscreen.SetActive(false);
    }
    public void backprofile()
    {
        homescreen();
        profilescreen.SetActive(false);
    }
    public void backnamechange()
    {
        settingscreen.SetActive(true);
        namechangescreen.SetActive(false);
    }
    public void backpasschange()
    {
        settingscreen.SetActive(true);
        passchangescreen.SetActive(false);
    }
    public void backemailchange()
    {
        settingscreen.SetActive(true);
        emailchangescreen.SetActive(false);
    }
    public void backabout()
    {
        homescreen();
        aboutscreen.SetActive(false);
    }
    public void backplayers()
    {
        homescreen();
        players.SetActive(false);
    }



    public void registeracc()
    {
        loginScene.SetActive(false);
        registerScene.SetActive(true);
    }
    public void resetacc()
    {
        loginScene.SetActive(false);
        resetScene.SetActive(true);
    }
    public void confirmacc()
    {
        resetScene.SetActive(false);
        registerScene.SetActive(false);
    }
    public void backconfirm()
    {
        resetScene.SetActive(true);
        registerScene.SetActive(false);
    }
    public void backregister()
    {
        loginScene.SetActive(true);
        registerScene.SetActive(false);
    }
    public void backreset()
    {
        loginScene.SetActive(true);
        resetScene.SetActive(false);
    }
}
